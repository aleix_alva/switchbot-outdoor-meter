# switchbot-outdoor-meter

Scripts to get data from Switchbot outdoor meters via Bluetooth.

bluetooth_alive.sh starts and keeps alive bluetooth services.
blutuz_launch.sh launches the previous script.
switchbot.sh retrieves data from the sensors.
sweetbot_launch.sh launches the previous script.

You need to edit these scripts and change the number of sensors, the paths to data, labels, etc.

You can also enable sending data through MQTT.

A possible crontab that works well for me is the following:

```
@reboot  /path/to/repo/./blutuz_launch.sh
*/60 * * * *  /path/to/repo/./blutuz_launch.sh
*/60 * * * *  /path/to/repo/./sweetbot_launch.sh
@reboot    /path/to/repo/./switchbot.sh > /dev/null 2>&1
```
