#!/bin/bash
# keep bluetooth alive
duerme=60
pkill -f "bluetoothctl"
while true;do
	bluetoothctl power on
	bluetoothctl agent on
	for ((i = 0 ; i < 60 ; i++)); do
		bluetoothctl --timeout 45 scan le
		sleep "$duerme"
		bluetoothctl --timeout 5 scan off
	done
	bluetoothctl power off
	pkill -f "bluetoothctl"
done
