#!/bin/bash
# script to retrieve data from the 3 switchbot sensors
# initialisation //+
duerme=10
datadir="/path/to/data/folder"
#server_ip="xx.xx.xx.xx" # ip of your server (for MQTT)
data_south="$datadir/south"
data_indoor="$datadir/indoor"
data_north="$datadir/north"
label="$(cat "/home/$USER/.label")" # edit
topic="label/$label/sbt" # edit as you wish
mac_south="XX:XX:XX:XX:XX:XX" # mac addresses
mac_indoor="XX:XX:XX:XX:XX:XX"
mac_north="XX:XX:XX:XX:XX:XX"
mac_south_2="xx xx xx xx xx xx" # repeat mac addresses
mac_indoor_2="xx xx xx xx xx xx"
mac_north_2="xx xx xx xx xx xx"
bat_pattern="77 00"
dev_south="TH-label-south"
dev_indoor="TH-label-indoor"
dev_north="TH-label-north"
tmpdir="/dev/shm/switchbot"
file_south="$tmpdir/south.txt"
file_indoor="$tmpdir/indoor.txt"
file_north="$tmpdir/north.txt"
cc_file_south="$tmpdir/cc_south.txt"
cc_file_indoor="$tmpdir/cc_indoor.txt"
cc_file_north="$tmpdir/cc_north.txt"
mkdir -p "$tmpdir"
mkdir -p "$data_south"
mkdir -p "$data_indoor"
mkdir -p "$data_north"
if [ ! -f "$cc_file_south" ]; then
	echo "0" > "$cc_file_south"
fi
if [ ! -f "$cc_file_indoor" ]; then
	echo "0" > "$cc_file_indoor"
fi
if [ ! -f "$cc_file_north" ]; then
	echo "0" > "$cc_file_north"
fi
# initialisation //-
while true;do
	# south //+
	mac="$mac_south"
	mac2="$mac_south_2"
	dev="$dev_south"
	file="$file_south"
	data_file="$data_south"
	cc_file="$cc_file_south"
	bluetoothctl info "$mac" > "$file"
	status="$?"
	#echo "south_status=$status"
	if [ "$status" -eq 0 ];then
		input=$(cat "$file" | grep "$mac2")
		#echo "south_input=$input"
		inputv=($input)
		# cc is the change counter
		cc_hex="${inputv[6]}"
		cc="$((16#$cc_hex))"
		# read the previous cc
		cc0=$(cat "$cc_file")
		#echo "south cc0=$cc0, cc=$cc"
		if [ "$cc" -ne "$cc0" ];then
			# only then we continue
			line=""
			now="$(date -Is)"
			today="$(date +%Y-%m-%d)"
			today_file="$data_file/$today.csv"
			echo "$now"
			echo "south:"
			echo "$cc" > "$cc_file"
			T1_hex="${inputv[9]}" # T units
			T2_hex="${inputv[8]}" # T decimal
			H_hex="${inputv[10]}" # H
			input2=$(cat "$file" | grep "$bat_pattern")
			input2v=($input2)
			bat_hex="${input2v[2]}" # battery status
			input3=$(cat "$file" | grep "RSSI")
			input3v=($input3)
			RSSI0="${input3v[2]}" # RSSI
			if [ -z "$RSSI" ]; then
				RSSI="-666"
			else
				RSSI=${RSSI0:1:${#RSSI0}-2}
			fi
			echo "RSSI = $RSSI dBm"
			if [ -n "$bat_hex" ]; then
				bat_dec=$((16#"$bat_hex"))
			fi
			echo "bat=$bat_dec %"
			H_dec=$((16#$H_hex))
			echo "H=$H_dec %"
			T2_dec=$(($((16#$T2_hex)) & 127))
			T1_dec=$(($((16#$T1_hex)) & 127))
			T_dec=$(echo "$T1_dec+0.1*$T2_dec" | bc -l)
			echo "T=$T_dec ºC"
			echo ""
			line+="$now,$T_dec,$H_dec,$bat_dec,$RSSI"
			#mosquitto_pub -h "$server_ip" -t "$topic/south" -m "$line"
			echo "$line" >> "$today_file"
		fi
	fi
	# south //-
	# indoor //+
	mac="$mac_indoor"
	mac2="$mac_indoor_2"
	dev="$dev_indoor"
	file="$file_indoor"
	data_file="$data_indoor"
	cc_file="$cc_file_indoor"
	bluetoothctl info "$mac" > "$file"
	status="$?"
	if [ "$status" -eq 0 ];then
		input=$(cat "$file" | grep "$mac2")
		#echo "$input"
		inputv=($input)
		# cc is the change counter
		cc_hex="${inputv[6]}"
		cc="$((16#$cc_hex))"
		# read the previous cc
		cc0=$(cat "$cc_file")
		if [ "$cc" -ne "$cc0" ];then
			# only then we continue
			line=""
			now="$(date -Is)"
			today="$(date +%Y-%m-%d)"
			today_file="$data_file/$today.csv"
			echo "$now"
			echo "indoor:"
			echo "$cc" > "$cc_file"
			T1_hex="${inputv[9]}" # T units
			T2_hex="${inputv[8]}" # T decimal
			H_hex="${inputv[10]}" # H
			input2=$(cat "$file" | grep "$bat_pattern")
			input2v=($input2)
			bat_hex="${input2v[2]}" # battery status
			input3=$(cat "$file" | grep "RSSI")
			input3v=($input3)
			RSSI0="${input3v[2]}" # RSSI
			if [ -z "$RSSI" ]; then
				RSSI="-666"
			else
				RSSI=${RSSI0:1:${#RSSI0}-2}
			fi
			echo "RSSI = $RSSI dBm"
			if [ -n "$bat_hex" ]; then
				bat_dec=$((16#"$bat_hex"))
			fi
			echo "bat = $bat_dec %"
			H_dec=$((16#$H_hex))
			echo "H = $H_dec %"
			T2_dec=$(($((16#$T2_hex)) & 127))
			T1_dec=$(($((16#$T1_hex)) & 127))
			T_dec=$(echo "$T1_dec+0.1*$T2_dec" | bc -l)
			echo "T = $T_dec ºC"
			echo ""
			line+="$now,$T_dec,$H_dec,$bat_dec,$RSSI"
			#mosquitto_pub -h "$server_ip" -t "$topic/indoor" -m "$line"
			echo "$line" >> "$today_file"
		fi
	fi
	# indoor //-
	# north //+
	mac="$mac_north"
	mac2="$mac_north_2"
	dev="$dev_north"
	file="$file_north"
	data_file="$data_north"
	cc_file="$cc_file_north"
	bluetoothctl info "$mac" > "$file"
	status="$?"
	if [ "$status" -eq 0 ];then
		input=$(cat "$file" | grep "$mac2")
		#echo "$input"
		inputv=($input)
		# cc is the change counter
		cc_hex="${inputv[6]}"
		cc="$((16#$cc_hex))"
		# read the previous cc
		cc0=$(cat "$cc_file")
		#echo "$cc, $cc0"
		# we use -ne because the internal counter may reset
		if [ "$cc" -ne "$cc0" ];then
			# only then we continue
			now="$(date -Is)"
			today="$(date +%Y-%m-%d)"
			today_file="$data_file/$today.csv"
			echo "$now"
			echo "north:"
			line=""
			echo "$cc" > "$cc_file"
			T1_hex="${inputv[9]}" # T units
			T2_hex="${inputv[8]}" # T decimal
			H_hex="${inputv[10]}" # H
			input2=$(cat "$file" | grep "$bat_pattern")
			input2v=($input2)
			bat_hex="${input2v[2]}" # battery status
			input3=$(cat "$file" | grep "RSSI")
			input3v=($input3)
			RSSI0="${input3v[2]}" # RSSI
			if [ -z "$RSSI" ]; then
				RSSI="-666"
			else
				RSSI=${RSSI0:1:${#RSSI0}-2}
			fi
			echo "RSSI = $RSSI dBm"
			if [ -n "$bat_hex" ]; then
				bat_dec=$((16#"$bat_hex"))
			fi
			echo "bat = $bat_dec %"
			H_dec=$((16#$H_hex))
			echo "H = $H_dec %"
			T2_dec=$(($((16#$T2_hex)) & 127))
			T1_dec=$(($((16#$T1_hex)) & 127))
			T_dec=$(echo "$T1_dec+0.1*$T2_dec" | bc -l)
			echo "T = $T_dec ºC"
			echo ""
			line+="$now,$T_dec,$H_dec,$bat_dec,$RSSI"
			#mosquitto_pub -h "$server_ip" -t "$topic/north" -m "$line"
			echo "$line" >> "$today_file"
		fi
	fi
	# north //-
	sleep "$duerme"
done
